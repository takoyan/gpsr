#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from std_msgs.msg import String
from gpsr.msg import Ans
import re
import numpy as np
import math
import difflib
import sys
from time import sleep
import os

def callback(data):
    print('ok')
    rate=rospy.Rate(10)
    if(data.place!=''):
        place=re.findall(r'[a-z]*\S', data.place)
        thing=re.findall(r'[a-z]*\S', data.thing)
        destination=re.findall(r'[a-z]*\S', data.destination)
        follow=re.findall(r'[a-z]*\S', data.follow)
        print(place)
        print(thing)
        print(destination)
        print(follow)

def gpsr_recv():
    rospy.init_node('gpsr_recv', anonymous=True)
    rospy.Subscriber('gpsr_deal', Ans, callback)
    rospy.spin()

if __name__=='__main__':
    gpsr_recv()
