#!/usr/bin/env python
# -*- coding: utf-8 -*-
import nltk
import numpy as np
import os
import glob
import rospy
from pocketsphinx import LiveSpeech, get_model_path
from std_msgs.msg import String
from gpsr.msg import Ans
import os
import sys


speak=['answer', 'tell', 'say', 'speak', 'talk', 'explain', 'teach', 'express', 'mouth', 'mention', 'utter']
go=['go', 'come', 'move', 'run', 'transfer', 'shift', 'travel']
carry=['carry', 'accompany', 'escort', 'deliver', 'bring', 'place', 'navigate', 'follow', 'locate', 'send', 'bear', 'channel', 'transport', 'transmit', 'submit']
find=['find', 'look', 'meet', 'get', 'pick', 'search', 'seek', 'hunt', 'watch']





"""
get_Index_of_Word関数
入力された文章内における,指定された単語の次に来る名詞のindex番号を返す.
"""
def get_Index_of_Word(sentence, word):
    global result
    index_of_word=sentence.index(word)
    for k in range(index_of_word+1, len(sentence)):
        if (result[k][1]=='NN' or
        result[k][1]=='NNS' or
        result[k][1]=='NNP' or
        result[k][1]=='NNPS' or
        result[k][1]=='PRP'):
            return k
    



"""
check_And関数
入力された文章内に'and'があるかどうかをチェックする.
'and'がない場合は要素数1の配列で返還.
'and'がある場合は要素数('and'の個数-1)の配列で返還.
いずれも各要素は'and'で区切られた文章である.
"""
def check_And(sentence):
    checked_sentence=sentence.split('and')
    return checked_sentence   #配列での返還




"""
separate_Verb関数
入力された文章内の動詞を分類する.
返り値として動詞の単語が返還される.
"""
def separate_Verb(sentence):
    for verbs in (speak, go, carry, find):
        for verb in verbs:
            if verb in sentence:
                return verb   #動詞の返還



            
def look_func(sentence, verb):
    ans=[]
    if 'from' in sentence: #fromが文中にある場合
        index_of_from=get_Index_of_Word(sentence, 'from')
        index_of_verb=get_Index_of_Word(sentence, verb)
        place=result[index_of_from][0] #場所を示す名詞を格納
        destination=result[index_of_verb][0] #目的地を示す名詞を格納
        index_of_dest=get_Index_of_Word(sentence, destination)
        thing=result[index_of_dest][0]
        ans.append(place)
        ans.append(thing)
        ans.append(destination)
        return ans

    if 'for' in sentence:
        index_of_for=get_Index_of_Word(sentence, 'for')
        thing=result[index_of_for][0]
        index_of_verb=get_Index_of_Word(sentence, thing)
        place=result[index_of_verb][0]
        ans.append(place)
        ans.append(thing)
        return ans



def carry_func(sentence, verb):
    ans=[]
    if 'from' in sentence:
        return look_func(sentence, verb)
        

    if 'to' in sentence:
        index_of_to=get_Index_of_Word(sentence, 'to')
        index_of_verb=get_Index_of_Word(sentence, verb)
        place=result[index_of_to][0]
        thing=result[index_of_verb][0]
        ans.append(place)
        ans.append(thing)
        return ans
    else:
        index_of_verb=get_Index_of_Word(sentence, verb)
        place=result[index_of_verb][0]
        index_of_next=get_Index_of_Word(sentence, place)
        thing=result[index_of_next][0]
        ans.append(place)
        ans.append(thing)
        return ans


def go_func(sentence, verb):
    ans=[]
    if 'to' in sentence:
        index_of_to=get_Index_of_Word(sentence, 'to')
        place=result[index_of_to][0]
        ans.append(place)
        return ans

    if 'after' in sentence:
        index_of_after=get_Index_of_Word(sentence, 'after')
        follow=result[index_of_after][0]
        if len(ans)<=0:
            ans=['empty']*4
            ans[3]=follow
        print(ans)
        return ans
        

def start_speech(data):
    global start_flag
    start_flag=True
    return 

def restart_speech(data):
    global flag
    flag=True
    return 


    

def gpsr_deal():
    #sentence='bring me this apple'
    #sentence='bring me this apple from the kitchen'
    #sentence='bring this apple to him'
    sentence='look for the fruit in the corridor'
    #sentence='go to the kitchen'
    #sentence='go after him'
    global start_flag
    global result
    rospy.init_node('gpsr_deal', anonymous=True)
    start_flag=False
    
    while(1):
        rospy.Subscriber('gpsr_ctrl', String, start_speech)
        if(start_flag==True):
            """音声認識を使う場合は要改良
            while(1):
            speech=LiveSpeech(dic='/home/takoyan/catkin_ws/src/spr/ziso.dict')
            for i in speech:
            sentence=i
            break
            break
            """
            break

    
    sentence=check_And(sentence) #文中のandの有無を確認
    sentence=sentence[0].split()#単語で区切る

    result={}#構文解析結果 [('Please', 'VB')]のような形
    result=nltk.pos_tag(sentence)
    print(result)

    verb=separate_Verb(sentence) #文中の動詞を獲得


    if verb in carry:
        answer=carry_func(sentence, verb)
    elif verb in find:
        answer=look_func(sentence, verb) #[place, thing]の順
    elif verb in go:
        answer=go_func(sentence, verb)

        
    place='empty'
    thing='empty'
    destination='empty'
    follow='empty'
    

    if len(answer)>=4:
        follow=answer[3]
    if len(answer)>=3:
        destination=answer[2]
    if len(answer)>=2:
        thing=answer[1]
    if len(answer)>=1:
        place=answer[0]
        


    pub=rospy.Publisher('gpsr_deal', Ans, queue_size=100)
    box=Ans()
    box.place=place
    box.thing=thing
    box.destination=destination
    box.follow=follow
    print(box)
    rospy.sleep(5)
    pub.publish(box)
    
    return


        
        

        
if __name__=='__main__':
    try:
        gpsr_deal()
    except rospy.ROSInterruptException:
        pass
